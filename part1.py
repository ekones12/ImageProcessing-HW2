import os
import cv2
import numpy as np
from transforms import Magnitude_Transform,detransform



def part1(imagelist):
    
    ################################################################
    ##   CONVERTING ORIGINAL IMAGE TO FREQUENCY DOMAIN TYPE       ##
    ###############################################################
    frequency_domain_original=Magnitude_Transform(imagelist[len(imagelist)-1],spectrum="Original Image")
     ######################################################################################
    j=1
    for i in range(len(imagelist)-1):
 
        ###############################################################
        ##   CONVERTING BLURED IMAGES TO FREQUENCY DOMAIN TYPE       ##
        frequency_domain_blur= Magnitude_Transform(imagelist[i])   ##
        ###############################################################
        ######################################################################################
        ##  CALCULATING BLURING KERNEL WITH FREQUENCY DOMAIN TYPE                           ##
        blur_kernel_frequency =np.divide(frequency_domain_blur,frequency_domain_original)  ##
        ######################################################################################
        ############################################################################################
        ## CALCULATING ORIGINAL IMAGE WITH BLURING KERNEL AND BLURED IMAGE                        ##
        calculated_original_frequency = np.divide(frequency_domain_blur,blur_kernel_frequency)   ##
        ## FFT TO IMAGE TRANSFORM FROM HERE                                                       ##  
        calculated_original = detransform(calculated_original_frequency).real.round().astype(np.uint8)
        Magnitude_Transform(calculated_original,spectrum="Deblured Image\nfrom blur"+str(j))
        
        print("Deblured blur"+str(j)+" is equal original? :",np.array_equal(calculated_original,imagelist[len(imagelist)-1]))
        ############################################################################################
        cv2.imwrite(os.path.join("results/part1","result_blur"+str(j)+".png"),calculated_original)
        j+=1
