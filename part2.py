import os
import cv2
import numpy as np
from transforms import Magnitude_Transform,padwithzeros,convolution_frequency_domain,motionblur



def part2(imagelist,image,kernel_datas):
    j=1
    for i in range(len(imagelist)):
        ###############################################################
        ##   CONVERTING BLURED IMAGES TO FREQUENCY DOMAIN TYPE         ##
        ##   ANALYZING BLUR MOTION EFFECT                              ##
        Magnitude_Transform(imagelist[i],spectrum = kernel_datas[i][2])              ##
        ###############################################################
        ####################################################################################################################################################################
        ## PREDICTION OF BLUR MOTION KERNEL ##
        blur_kernel = motionblur(kernel_datas[i][0],kernel_datas[i][1],image.shape)
        #####################################################################################################################################################################
        ## CONVOLUTION  ORIGINAL IMAGE  AND PREDICTED KERNEL
        result = convolution_frequency_domain(image,blur_kernel,kernel_name=kernel_datas[i][2])
        
        cv2.imwrite(os.path.join("results/part2","result_blur"+str(j)+".png"),result)
        j+=1
        

