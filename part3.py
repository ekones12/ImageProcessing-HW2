import os
import cv2
import numpy as np
from transforms import Magnitude_Transform,detransform,padwithzeros,motionblur,deconvulation,special





def part3(imagelist,names,treshold):
    j=1

    for i in range(len(imagelist)):
        ###############################################################
        ##   CONVERTING BLURED IMAGES TO FREQUENCY DOMAIN TYPE       ##
        ## ANALYZING BLUR MOTION EFFECT                              ##
        Magnitude_Transform(imagelist[i],spectrum=names[i][2])##
        ###############################################################
        ## PREDICTION OF BLUR MOTION KERNEL AND GET FREQUENCY DOMAIN
        blur_kernel = motionblur(names[i][0],names[i][1],imagelist[i].shape)
        ## DECONVOLUTION IMAGE WITH PREDICTED KERNEL
        result= deconvulation(imagelist[i],blur_kernel,treshold=treshold)
        if treshold == None:
            cv2.imwrite(os.path.join("results/part3a","result_blur"+str(j)+".png"),result)
        else:
            cv2.imwrite(os.path.join("results/part3b","result_blur"+str(j)+".png"),result)
        j+=1
        
       

def part3bonus(imagelist,names,imagename):
    j=1

    for i in range(len(imagelist)):
        ###############################################################
        ##   CONVERTING BLURED IMAGES TO FREQUENCY DOMAIN TYPE       ##
        ## ANALYZING BLUR MOTION EFFECT                              ##
        imagelist[i] = np.divide(imagelist[i],255)
        ###############################################################
        ## PREDICTION OF BLUR MOTION KERNEL AND GET FREQUENCY DOMAIN
        blur_kernel = motionblur(names[i][0],names[i][1],imagelist[i].shape)
        # RESTORED WITH MODIFICATED RICHARDSON LUCY
        result= special(imagelist[i],blur_kernel,40,imagename+str(j))
        cv2.imwrite(os.path.join("results/part3c","result_blur"+str(j)+".png"),result)
        j+=1
