import os
import cv2
from part1 import part1
from part2 import part2
from part3 import part3,part3bonus
from part4 import part4

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)



    
def read_images(image_list,part):
    imagelist=[]
    imagelist.append(cv2.imread(image_list[0],0))
    imagelist.append(cv2.imread(image_list[1],0))
    imagelist.append(cv2.imread(image_list[2],0))
    if part==1:
        imagelist.append(cv2.imread(image_list[3],0))
    return imagelist



    

def main():

    
    createFolder("results/part1")
    createFolder("results/part2")
    createFolder("results/part3a")
    createFolder("results/part3b")
    createFolder("results/part3c")
    createFolder("results/part4")
    
    part1_image_names=["part1/blur1.png","part1/blur2.png","part1/blur3.png","part1/original.jpg"]
    part2_image_names=["part2/blur1.png","part2/blur2.png","part2/blur3.png"]
    part2_angle_lengths=[[21,90,"blur1"],[21,135,"blur2"],[15,180,"blur3"]]
    
    imagelist1=read_images(part1_image_names,1)
    imagelist2=read_images(part2_image_names,2)
    
    
    print("***********************************************part1 Results*************************************\n")
    part1(imagelist1)
    print("**************************************************************************************************\n")
    print("\n\n ********************************************part2 Results*************************************\n")
    part2(imagelist2,imagelist1[3],part2_angle_lengths)
    print("**************************************************************************************************\n")
    print("\n\n ********************************************part3a Results*************************************\n")
    part3(imagelist2,part2_angle_lengths,None)
    print("**************************************************************************************************\n")
    print("\n\n ********************************************part3b Results**************************************\n")
    part3(imagelist2,part2_angle_lengths,0.05)
    print("**************************************************************************************************\n")
    print("\n\n ********************************************part3c Results**************************************\n")
    part3bonus(imagelist2,part2_angle_lengths,"blur")
    print("**************************************************************************************************\n")
    print("\n\n ********************************************part4 Result****************************************\n")
    part4(40)
    cv2.waitKey()
    cv2.destroyAllWindows()
    
    
main()