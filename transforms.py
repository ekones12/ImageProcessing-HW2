import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import convolve2d as conv2
import cv2

def detransform(magnitude):
  
    inversefft = np.fft.ifftshift(magnitude)
    image = np.fft.ifft2(inversefft)
    return image


def convolution_frequency_domain(image,kernel,kernel_name=""):
    ###########################################
    ## image to frequency domain
    freq_image = np.fft.fft2(image)
    ## image shifted 
    shift_image=np.fft.fftshift(freq_image)
    ## kernel to frequency domain
    freq_kernel = np.fft.fft2(kernel)
    ## kernel shifted
    shift_kernel=np.fft.fftshift(freq_kernel)
    ## Convolution theorem in here 
    freq_blured_image = shift_image*shift_kernel 
    ## blured image transform to spatial domain
    result = detransform(freq_blured_image).real.round().astype(np.uint8)
    ############################################
    if kernel_name !="":
        plt.subplot(1,5,3)
        plt.imshow(image,cmap="gray")
        plt.title('Test Image ')
        
        plt.subplot(1,5,4)
        plt.imshow(result,cmap="gray") # the imaginary part is an artifact
        plt.title('Blured-Test\nwith '+kernel_name+"-kernel")
        
        plt.subplot(1,5,5)
        plt.imshow(10*np.log(1+np.abs(np.fft.fftshift(np.fft.fft2(result)))),cmap="gray") # the imaginary part is an artifact
        plt.title('Blured-Test Magnitude \n'+kernel_name)
        plt.show()
    return result



def deconvulation(image,kernel,treshold=None):
    ###########################################
    ## image to frequency domain
    freq_image = np.fft.fft2(image)
    ## image shifted 
    shift_image=np.fft.fftshift(freq_image)
    ## kernel to frequency domain
    freq_kernel = np.fft.fft2(kernel)
    ## kernel shifted
    shift_kernel=np.fft.fftshift(freq_kernel)
    
    if treshold!=None:
        points = np.abs(shift_kernel)>=treshold
        zeros  =np.abs(shift_kernel)< treshold
        shift_image[points]= shift_image[points]/shift_kernel[points]
        shift_image[zeros]=0
        freq_blured_image=shift_image
        result = detransform(freq_blured_image).real 
    else:
        ## deConvolution theorem in here 
        shift_kernel[shift_kernel==0]=1
        freq_blured_image = np.divide(shift_image,shift_kernel)
        result = detransform(freq_blured_image).real.round().astype(np.uint8)
    ############################################
    plt.subplot(1,5,3)
    plt.imshow(image,cmap="gray")
    plt.title('Test Image ')
    
    plt.subplot(1,5,4)
    plt.imshow(result,cmap="gray") # the imaginary part is an artifact
    plt.title('deBlured-Test\n with-predicted kernel')
    
    plt.subplot(1,5,5)
    plt.imshow(10*np.log(1+np.abs(np.fft.fftshift(np.fft.fft2(result.round().astype(np.uint8))))),cmap="gray") # the imaginary part is an artifact
    plt.title('deBlured-Test\n Magnitude Spectrum')
    plt.show()
    return result





def myRichardson_lucy(image,kernel,iteration):
    
    kernel_padded = np.pad(kernel, (((image.shape[0]-kernel.shape[0])//2+1,
                              (image.shape[0]-kernel.shape[0])//2),
                             ((image.shape[1]-kernel.shape[0])//2+1,
                              (image.shape[1]-kernel.shape[0])//2)), padwithzeros)
    ## firstly kernel inverse shifted becuse zero padded
    kernel_padded = np.fft.ifftshift(kernel_padded)
    
    kernel_fft=np.fft.fft2(kernel_padded)
    
    richard = np.ones(image.shape)
    richard[:] = 0.5
    
    for i in range(iteration):
        
        richard_fft=np.fft.fft2(richard)
        divide= richard_fft*kernel_fft
        divide=np.fft.ifft2(divide)
        divide=image/divide
        
        divide = np.fft.fft2(divide)
        divide= divide*kernel_fft
        divide=np.fft.ifft2(divide)
        richard = np.multiply(richard,divide)
        
    result = np.uint8(np.clip(richard.real * 255, 0, 255))
    plt.figure(figsize=(15,15))
    ############################################
    plt.subplot(1,4,1)
    plt.imshow(image,cmap="gray")
    plt.title('Astro Image ')
    ############################################
    plt.subplot(1,4,2)
    plt.imshow(10*np.log(1+np.abs(np.fft.fftshift(np.fft.fft2(image)))),cmap="gray") # the imaginary part is an artifact
    plt.title('Astro Image \nMagnitude Spectrum')
    ##############################################3
    plt.subplot(1,4,3)
    plt.imshow(result,cmap="gray") # the imaginary part is an artifact
    plt.title('Astro Image \nafter Richardson_Lucy')
    ###############################################
    plt.subplot(1,4,4)
    plt.imshow(10*np.log(1+np.abs(np.fft.fftshift(np.fft.fft2(result)))),cmap="gray") # the imaginary part is an artifact
    plt.title('Astro Image \nafter Richardson_Lucy \nMagnitude Spectrum')
    plt.show()
    ###############################################
    return result





def motionblur(lenght,thetha,total):
    nump=np.diag(np.zeros(lenght))
    if thetha == 0 or thetha == 180:
        for i in range(lenght):
            for j in range(lenght):
                
                if j == ((lenght+1)/2):
                    nump[i][j]=1
                else:
                    nump[i][j]=0
    if thetha == 135:
        
        for i in range(lenght):
            for j in range(lenght):
                if i == j:
                    nump[i][j]=1
                else:
                    nump[i][j]=0
    if thetha == 45:
        for i in range(lenght):
            for j in range(lenght):
                if i == (lenght-1)-j:
                    nump[i][j]=1
                else:
                    nump[i][j]=0
                    
    if thetha == 90 :
        for i in range(lenght):
            for j in range(lenght):
                if i == ((lenght+1)/2):
                    nump[i][j]=1
                else:
                    nump[i][j]=0
    
    nump = np.pad(nump, (((total[0]-lenght)//2,(total[0]-lenght)//2+1), ((total[1]-lenght)//2,(total[1]-lenght)//2+1)), padwithzeros)
    ## predicted kernel inverse shifted becuse zero padded
    nump = np.fft.ifftshift(nump)
    return nump/np.sum(nump)

def padwithzeros(vector, pad_width, iaxis, kwargs):
    vector[:pad_width[0]] = 0
    vector[-pad_width[1]:] = 0
    return vector



   
def Magnitude_Transform(image,spectrum=False):
    fft2 = np.fft.fft2(image)
    fftfshift = np.fft.fftshift(fft2)
    magnitude_spectrum_gray = fftfshift

    if spectrum!=False:
        plt.figure(figsize=(20,15))
        magnitutde_plot = 10*np.log(1+np.abs(magnitude_spectrum_gray))
    
        plt.subplot(1,5,1)
        plt.imshow(image,cmap="gray")
        plt.title(spectrum)
        
        plt.subplot(1,5,2)
        plt.imshow(magnitutde_plot,cmap="gray")
        plt.title('Magnitude Spectrum \n'+spectrum)
        
    return magnitude_spectrum_gray



def special(image,kernel,iteration,name):

    
    kernel_fft=np.fft.fft2(kernel)
    
    richard = np.ones(image.shape)
    richard[:] = 0.5
    
    for i in range(iteration):
        # for regularization ricardson-lucy
        richard+=1
        richard_fft=np.fft.fft2(richard)
        divide= richard_fft*kernel_fft
        
        divide=np.fft.ifft2(divide)
        # for regularization ricardson-lucy
        divide=(image+1)/divide
        
        divide = np.fft.fft2(divide)
        divide= divide*kernel_fft
        divide=np.fft.ifft2(divide)
        # for regularization ricardson-lucy
        richard = np.multiply(richard,divide)-1
        
        
        
    result = np.uint8(np.clip(richard.real*255, 0, 255))
    plt.figure(figsize=(15,15))
    ############################################
    plt.subplot(1,4,1)
    plt.imshow(image,cmap="gray")
    plt.title(name +" Image")
    ############################################
    plt.subplot(1,4,2)
    plt.imshow(10*np.log(1+np.abs(np.fft.fftshift(np.fft.fft2(image)))),cmap="gray") # the imaginary part is an artifact
    plt.title(name+' Image \nMagnitude Spectrum')
    ##############################################3
    plt.subplot(1,4,3)
    plt.imshow(result,cmap="gray") # the imaginary part is an artifact
    plt.title(name+' Image after\n Modificated Richardson_Lucy')
    ###############################################
    plt.subplot(1,4,4)
    plt.imshow(10*np.log(1+np.abs(np.fft.fftshift(np.fft.fft2(result)))),cmap="gray") # the imaginary part is an artifact
    plt.title(name+' Image after \nModificated Richardson_Lucy \nMagnitude Spectrum')
    plt.show()
    ###############################################
    return result
