"""
This code piece is taken from: http://scikit-image.org/docs/dev/auto_examples/filters/plot_deconvolution.html
"""
import numpy as np
from scipy.signal import convolve2d as conv2
from scipy import ndimage
from skimage import color, data
from transforms import myRichardson_lucy
import cv2
import os

def part4(iterate):
    
    astro = color.rgb2gray(data.astronaut())
    
    psf = np.ones((5,5)) / 25
    astro = conv2(astro, psf, 'same')
    # Add Noise to Image
    astro_noisy = astro.copy()
    astro_noisy += (np.random.poisson(lam=25, size=astro.shape) - 10) / 255.
    
    # Restore Image using Richardson-Lucy algorithm. Implement the algorithm in frequency domain.
    # Determine the iteration number empirically, give your results in the report.
    deconvolved = myRichardson_lucy(astro_noisy, psf,iterate)
    cv2.imwrite(os.path.join("results/part4","deblurpart4.png"),deconvolved)